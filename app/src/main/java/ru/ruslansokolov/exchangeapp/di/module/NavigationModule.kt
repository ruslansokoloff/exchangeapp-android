package ru.ruslansokolov.exchangeapp.di.module

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import dagger.Module
import dagger.Provides
import ru.ruslansokolov.core.di.PerApplication

@Module
class NavigationModule(private val cicerone: Cicerone<Router>) {

    @PerApplication
    @Provides
    fun provideRouter(): Router {
        return cicerone.router
    }

    @PerApplication
    @Provides
    fun provideNavigatorHolder(): NavigatorHolder {
        return cicerone.getNavigatorHolder()
    }
}