package ru.ruslansokolov.exchangeapp.di.component

import dagger.Component
import ru.ruslansokolov.core.di.PerApplication
import ru.ruslansokolov.exchangeapp.di.module.*
import ru.ruslansokolov.exchangeapp.presentation.activity.MainActivity
import ru.ruslansokolov.exchangeapp.presentation.screen.currencies_list.CurrenciesListFragment
import ru.ruslansokolov.exchangeapp.presentation.screen.favorite_currencies.FavoriteCurrenciesFragment

@PerApplication
@Component(
    modules = [
        ApplicationModule::class,
        GsonModule::class,
        HttpModule::class,
        DatabaseModule::class,
        NavigationModule::class,
        CurrenciesModule::class,
    ]
)
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: CurrenciesListFragment)
    fun inject(activity: FavoriteCurrenciesFragment)
}