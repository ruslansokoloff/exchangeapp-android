package ru.ruslansokolov.exchangeapp.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.ruslansokolov.core.di.PerApplication
import ru.ruslansokolov.exchangeapp.data.database.AppDatabase

@Module(includes = [ApplicationModule::class])
class DatabaseModule {

    @PerApplication
    @Provides
    fun provideDatabase(appContext: Context): AppDatabase {
        return Room.databaseBuilder(appContext, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
}