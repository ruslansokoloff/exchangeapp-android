package ru.ruslansokolov.exchangeapp.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.ruslansokolov.core.di.PerApplication
import ru.ruslansokolov.exchangeapp.data.network.AccessKeyInterceptor
import java.util.concurrent.TimeUnit

@Module(includes = [GsonModule::class])
class HttpModule {

    @PerApplication
    @Provides
    fun provideAccessKeyInterceptor(): AccessKeyInterceptor {
        return AccessKeyInterceptor()
    }

    @PerApplication
    @Provides
    fun provideHttpClient(accessKeyInterceptor: AccessKeyInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .addInterceptor(accessKeyInterceptor)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(
                CONNECTION_TIMEOUT,
                CONNECTION_TIMEOUT_UNIT
            )
            .readTimeout(
                CONNECTION_TIMEOUT,
                CONNECTION_TIMEOUT_UNIT
            )
            .writeTimeout(
                CONNECTION_TIMEOUT,
                CONNECTION_TIMEOUT_UNIT
            )
            .dispatcher(Dispatcher().apply {
                maxRequests = 1
            })

        return builder.build()
    }

    @PerApplication
    @Provides
    fun provideRetrofitBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    private companion object {
        const val CONNECTION_TIMEOUT = 30L
        val CONNECTION_TIMEOUT_UNIT = TimeUnit.SECONDS
    }
}