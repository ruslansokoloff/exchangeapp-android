package ru.ruslansokolov.exchangeapp.di.module

import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import ru.ruslansokolov.core.di.PerApplication

@Module
class ApplicationModule(private val appContext: Context) {

    @PerApplication
    @Provides
    fun provideAppContext(): Context = appContext

    @PerApplication
    @Provides
    fun provideResources(): Resources = appContext.resources
}