package ru.ruslansokolov.exchangeapp.di.module

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import ru.ruslansokolov.core.di.PerApplication
import ru.ruslansokolov.exchangeapp.BuildConfig
import ru.ruslansokolov.exchangeapp.data.currency_repository.CurrenciesRepository
import ru.ruslansokolov.exchangeapp.data.currency_repository.CurrenciesRepositoryImpl
import ru.ruslansokolov.exchangeapp.data.database.AppDatabase
import ru.ruslansokolov.exchangeapp.data.mapper.CurrencyMapper
import ru.ruslansokolov.exchangeapp.data.mapper.CurrencyMapperImpl
import ru.ruslansokolov.exchangeapp.data.network.CurrenciesApi

@Module(
    includes = [
        HttpModule::class,
        DatabaseModule::class
    ]
)
class CurrenciesModule {

    @PerApplication
    @Provides
    fun provideCurrenciesMapper(): CurrencyMapper {
        return CurrencyMapperImpl()
    }

    @PerApplication
    @Provides
    fun provideCurrencyApi(client: OkHttpClient, retrofitBuilder: Retrofit.Builder): CurrenciesApi {
        return retrofitBuilder
            .client(client)
            .baseUrl(BuildConfig.API_BASE_URL)
            .build()
            .create(CurrenciesApi::class.java)
    }

    @PerApplication
    @Provides
    fun provideCurrencyRepository(
        currencyMapper: CurrencyMapper,
        database: AppDatabase,
        currenciesApi: CurrenciesApi
    ): CurrenciesRepository {
        return CurrenciesRepositoryImpl(
            currencyMapper,
            database.currencyDao(),
            database.currencyFavoriteDao(),
            database.currencySelectedDao(),
            currenciesApi,
        )
    }
}