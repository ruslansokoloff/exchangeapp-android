package ru.ruslansokolov.exchangeapp.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import ru.ruslansokolov.core.di.PerApplication

@Module
class GsonModule {

    @PerApplication
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .serializeNulls()
            .enableComplexMapKeySerialization()
            .create()
    }
}