package ru.ruslansokolov.exchangeapp

import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDex
import com.github.terrakok.cicerone.Cicerone
import ru.ruslansokolov.exchangeapp.di.component.AppComponent
import ru.ruslansokolov.exchangeapp.di.component.DaggerAppComponent
import ru.ruslansokolov.exchangeapp.di.module.ApplicationModule
import ru.ruslansokolov.exchangeapp.di.module.NavigationModule

class ExchangeApp : Application() {

    lateinit var appComponent: AppComponent

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .applicationModule(ApplicationModule(this))
            .navigationModule(NavigationModule(Cicerone.create()))
            .build()
    }
}

fun Context.appComponent(): AppComponent {
    return (applicationContext as ExchangeApp).appComponent
}

fun Fragment.appComponent(): AppComponent {
    return requireContext().appComponent()
}