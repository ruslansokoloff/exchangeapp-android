package ru.ruslansokolov.exchangeapp.presentation.screen.currencies_list

import android.app.AlertDialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import ru.ruslansokolov.core.presentation.fragment.BaseFragment
import ru.ruslansokolov.exchangeapp.BR
import ru.ruslansokolov.exchangeapp.R
import ru.ruslansokolov.exchangeapp.appComponent
import ru.ruslansokolov.exchangeapp.data.entities.CurrenciesSortType
import ru.ruslansokolov.exchangeapp.databinding.FragmentCurrenciesListBinding
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyAdapter
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListRenderer
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListState
import javax.inject.Inject

class CurrenciesListFragment :
    BaseFragment<CurrenciesListViewModel, FragmentCurrenciesListBinding>(CurrenciesListViewModel::class.java) {

    companion object {
        fun newInstance(): CurrenciesListFragment {
            return CurrenciesListFragment()
        }
    }

    override val layoutResId: Int = R.layout.fragment_currencies_list

    override val viewModelVariableId: Int = BR.viewModel

    @Inject
    lateinit var currenciesListRenderer: CurrenciesListRenderer

    private lateinit var adapter: CurrencyAdapter

    override fun inject() {
        appComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setToolbarTitle(R.string.home)
        setToolbarMenu(R.menu.menu_sort)
        initAdapter()

        viewModel.state.observe(viewLifecycleOwner, this::render)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.nav_sort) {
            showSortDialog()
        }
        return true
    }

    private fun initAdapter() {
        adapter = CurrencyAdapter()
        adapter.favoriteClickListener = viewModel.favoriteClickListener
        adapter.selectClickListener = viewModel.selectClickListener
        binding.currenciesLayout.currenciesRecyclerView.adapter = adapter
    }

    private fun render(state: CurrenciesListState) {
        when (state) {
            is CurrenciesListState.Loading -> {
                setToolbarTitle(R.string.loading)
            }
            is CurrenciesListState.Error -> {
                setToolbarTitle(R.string.home)
            }
            is CurrenciesListState.Success -> {
                val selected = state.currencies.firstOrNull { it.currency.selected }
                setToolbarTitle(selected?.displayName)
            }
        }

        currenciesListRenderer.render(binding.currenciesLayout, state, adapter)
    }

    private fun showSortDialog() {
        val items = CurrenciesSortType.values()
            .map { it.toString() }
            .toTypedArray()

        AlertDialog.Builder(context)
            .setItems(items) { _, which ->
                viewModel.onSortChanged(CurrenciesSortType.values()[which])
            }
            .show()
    }
}