package ru.ruslansokolov.exchangeapp.presentation.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.github.terrakok.cicerone.androidx.FragmentScreen
import ru.ruslansokolov.exchangeapp.presentation.screen.currencies_list.CurrenciesListFragment
import ru.ruslansokolov.exchangeapp.presentation.screen.favorite_currencies.FavoriteCurrenciesFragment

object Screens {

    fun currenciesList() = object : FragmentScreen {
        override fun createFragment(factory: FragmentFactory): Fragment {
            return CurrenciesListFragment.newInstance()
        }
    }

    fun favoriteCurrencies() = object : FragmentScreen {
        override fun createFragment(factory: FragmentFactory): Fragment {
            return FavoriteCurrenciesFragment.newInstance()
        }
    }
}