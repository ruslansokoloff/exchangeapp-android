package ru.ruslansokolov.exchangeapp.presentation.currency_adapter

import ru.ruslansokolov.core.presentation.adapter.BaseBindingViewHolder
import ru.ruslansokolov.exchangeapp.BR
import ru.ruslansokolov.exchangeapp.databinding.ItemCurrencyBinding

class CurrencyViewHolder(
    binding: ItemCurrencyBinding
) : BaseBindingViewHolder<CurrencyItem, ItemCurrencyBinding>(binding) {

    override val viewModelVariableId: Int = BR.item
}