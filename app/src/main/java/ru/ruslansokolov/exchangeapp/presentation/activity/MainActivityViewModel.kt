package ru.ruslansokolov.exchangeapp.presentation.activity

import android.os.Bundle
import com.github.terrakok.cicerone.Router
import ru.ruslansokolov.core.presentation.activity.ActivityViewModel
import ru.ruslansokolov.core.presentation.view_model.RxViewModelDelegate
import ru.ruslansokolov.exchangeapp.R
import ru.ruslansokolov.exchangeapp.presentation.navigation.Screens
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
    private val router: Router
) : ActivityViewModel<RxViewModelDelegate>() {

    override fun onViewModelCreated(args: Bundle?) {
        super.onViewModelCreated(args)

        router.newRootScreen(Screens.currenciesList())
    }

    fun onMenuItemClicked(id: Int) {
        val screen = when (id) {
            R.id.nav_home -> Screens.currenciesList()
            R.id.nav_favorites -> Screens.favoriteCurrencies()
            else -> null
        } ?: return

        router.newRootScreen(screen)
    }
}