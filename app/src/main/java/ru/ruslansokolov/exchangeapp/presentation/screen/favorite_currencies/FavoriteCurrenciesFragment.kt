package ru.ruslansokolov.exchangeapp.presentation.screen.favorite_currencies

import android.os.Bundle
import android.view.View
import ru.ruslansokolov.core.presentation.fragment.BaseFragment
import ru.ruslansokolov.exchangeapp.BR
import ru.ruslansokolov.exchangeapp.R
import ru.ruslansokolov.exchangeapp.appComponent
import ru.ruslansokolov.exchangeapp.databinding.FragmentFavoriteCurrenciesBinding
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyAdapter
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListRenderer
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListState
import javax.inject.Inject

class FavoriteCurrenciesFragment :
    BaseFragment<FavoriteCurrenciesViewModel, FragmentFavoriteCurrenciesBinding>(
        FavoriteCurrenciesViewModel::class.java
    ) {

    companion object {
        fun newInstance(): FavoriteCurrenciesFragment {
            return FavoriteCurrenciesFragment()
        }
    }

    override val layoutResId: Int = R.layout.fragment_favorite_currencies

    override val viewModelVariableId: Int = BR.viewModel

    @Inject
    lateinit var currenciesListRenderer: CurrenciesListRenderer

    private lateinit var adapter: CurrencyAdapter

    override fun inject() {
        appComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setToolbarTitle(R.string.favorites)
        initAdapter()

        viewModel.state.observe(viewLifecycleOwner, this::render)
    }

    private fun initAdapter() {
        adapter = CurrencyAdapter()
        adapter.favoriteClickListener = viewModel.favoriteClickListener
        adapter.selectClickListener = viewModel.selectClickListener
        binding.currenciesLayout.currenciesRecyclerView.adapter = adapter
    }

    private fun render(state: CurrenciesListState) {
        currenciesListRenderer.render(binding.currenciesLayout, state, adapter)
    }
}