package ru.ruslansokolov.exchangeapp.presentation.entities

import io.reactivex.Single
import ru.ruslansokolov.core.presentation.text_config.StringTextConfig
import ru.ruslansokolov.exchangeapp.data.entities.Currency
import javax.inject.Inject

class CurrenciesListStateFactory @Inject constructor(
    private val mapper: CurrenciesListStateMapper
) {

    operator fun invoke(input: Single<List<Currency>>): Single<CurrenciesListState> {
        return input
            .map(mapper::invoke)
            .onErrorReturn {
                CurrenciesListState.Error(StringTextConfig(it.localizedMessage))
            }
    }
}