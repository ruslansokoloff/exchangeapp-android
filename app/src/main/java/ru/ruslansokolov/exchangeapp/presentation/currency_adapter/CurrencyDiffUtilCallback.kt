package ru.ruslansokolov.exchangeapp.presentation.currency_adapter

import ru.ruslansokolov.core.presentation.adapter.BaseDiffUtilCallback

class CurrencyDiffUtilCallback : BaseDiffUtilCallback<CurrencyItem>() {

    override fun areItemsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
        return oldItem.currency.name == newItem.currency.name
    }

    override fun areContentsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean {
        return oldItem == newItem
    }
}