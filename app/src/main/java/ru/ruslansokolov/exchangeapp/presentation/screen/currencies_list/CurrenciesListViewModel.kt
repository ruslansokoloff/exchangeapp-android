package ru.ruslansokolov.exchangeapp.presentation.screen.currencies_list

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.ruslansokolov.core.presentation.fragment.FragmentViewModel
import ru.ruslansokolov.core.presentation.view_model.RxViewModelDelegate
import ru.ruslansokolov.exchangeapp.data.currency_repository.CurrenciesRepository
import ru.ruslansokolov.exchangeapp.data.entities.CurrenciesSortType
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyItem
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListState
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListStateFactory
import javax.inject.Inject

class CurrenciesListViewModel @Inject constructor(
    private val currenciesRepository: CurrenciesRepository,
    private val currenciesListStateFactory: CurrenciesListStateFactory,
) : FragmentViewModel<RxViewModelDelegate>() {

    val state: MutableLiveData<CurrenciesListState> = MutableLiveData(CurrenciesListState.Loading)

    val favoriteClickListener: (CurrencyItem) -> Unit = { item ->
        setFavorite(item.currency.name, item.isFavorite.value ?: false)
    }

    val selectClickListener: (CurrencyItem) -> Unit = { item ->
        setSelected(item.currency.name)
    }

    private var sortType: CurrenciesSortType = CurrenciesSortType.DEFAULT

    override fun onViewModelCreated(args: Bundle?) {
        super.onViewModelCreated(args)
        loadCurrencies(forceUpdate = false) {
            loadCurrencies(forceUpdate = true)
        }
    }

    fun onSortChanged(sortType: CurrenciesSortType) {
        this.sortType = sortType
        loadCurrencies(forceUpdate = false)
    }

    private fun loadCurrencies(forceUpdate: Boolean, successCallback: (() -> Unit)? = null) {
        currenciesListStateFactory(currenciesRepository.getAll(forceUpdate, sortType))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                this.state.value = state
                successCallback?.invoke()
            }
            .also { delegate.addDisposable(it) }
    }

    private fun setFavorite(currencyName: String, isFavorite: Boolean) {
        currenciesRepository.setFavorite(currencyName, isFavorite)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorComplete()
            .subscribe()
            .also { delegate.addDisposable(it) }
    }

    private fun setSelected(currencyName: String) {
        currenciesRepository.setSelectedCurrency(currencyName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                loadCurrencies(false)
            }
            .also { delegate.addDisposable(it) }
    }
}