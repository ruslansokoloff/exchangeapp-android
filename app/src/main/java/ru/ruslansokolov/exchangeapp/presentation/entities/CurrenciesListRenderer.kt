package ru.ruslansokolov.exchangeapp.presentation.entities

import androidx.core.view.isVisible
import ru.ruslansokolov.exchangeapp.databinding.IncCurrenciesListBinding
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyAdapter
import javax.inject.Inject

class CurrenciesListRenderer @Inject constructor() {

    fun render(
        binding: IncCurrenciesListBinding,
        state: CurrenciesListState,
        adapter: CurrencyAdapter
    ) {
        binding.apply {
            when (state) {
                is CurrenciesListState.Loading -> {
                    progress.isVisible = true
                    currenciesRecyclerView.isVisible = false
                    errorTextView.isVisible = false
                }
                is CurrenciesListState.Error -> {
                    progress.isVisible = false
                    currenciesRecyclerView.isVisible = false
                    errorTextView.isVisible = true
                    errorTextView.text = state.textConfig.getString(binding.root.context)
                }
                is CurrenciesListState.Success -> {
                    progress.isVisible = false
                    errorTextView.isVisible = false
                    currenciesRecyclerView.isVisible = true
                    adapter.updateItems(state.currencies)
                }
            }
        }

    }

}