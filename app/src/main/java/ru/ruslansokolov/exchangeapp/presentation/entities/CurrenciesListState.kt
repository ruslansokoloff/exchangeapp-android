package ru.ruslansokolov.exchangeapp.presentation.entities

import ru.ruslansokolov.core.presentation.text_config.TextConfig
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyItem

sealed class CurrenciesListState {

    object Loading : CurrenciesListState()

    class Error(val textConfig: TextConfig) : CurrenciesListState()

    class Success(val currencies: List<CurrencyItem>) : CurrenciesListState()
}