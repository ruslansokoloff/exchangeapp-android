package ru.ruslansokolov.exchangeapp.presentation.screen.favorite_currencies

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.ruslansokolov.core.presentation.fragment.FragmentViewModel
import ru.ruslansokolov.core.presentation.view_model.RxViewModelDelegate
import ru.ruslansokolov.exchangeapp.data.currency_repository.CurrenciesRepository
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyItem
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListState
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListStateFactory
import ru.ruslansokolov.exchangeapp.presentation.entities.CurrenciesListStateMapper
import javax.inject.Inject

class FavoriteCurrenciesViewModel @Inject constructor(
    private val currenciesRepository: CurrenciesRepository,
    private val currenciesListStateFactory: CurrenciesListStateFactory,
    private val currenciesListStateMapper: CurrenciesListStateMapper,
) : FragmentViewModel<RxViewModelDelegate>() {

    val state: MutableLiveData<CurrenciesListState> = MutableLiveData(CurrenciesListState.Loading)

    val favoriteClickListener: (CurrencyItem) -> Unit = { item ->
        setFavorite(item.currency.name)
    }

    val selectClickListener: (CurrencyItem) -> Unit = { item ->
        setSelected(item.currency.name)
    }

    override fun onViewModelCreated(args: Bundle?) {
        super.onViewModelCreated(args)
        loadCurrencies()
    }

    private fun loadCurrencies() {
        currenciesListStateFactory(currenciesRepository.getFavorites())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                this.state.value = state
            }
            .also { delegate.addDisposable(it) }
    }

    private fun setFavorite(currencyName: String) {
        currenciesRepository.setFavorite(currencyName, false)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorComplete()
            .subscribe {
                onItemRemoved(currencyName)
            }
            .also { delegate.addDisposable(it) }
    }

    private fun onItemRemoved(name: String) {
        val currentState = state.value
        if (currentState is CurrenciesListState.Success) {
            val currencies = ArrayList(currentState.currencies.map { it.currency })
            currencies.removeAll { it.name == name }
            state.value = currenciesListStateMapper(currencies)
        }
    }

    private fun setSelected(currencyName: String) {
        currenciesRepository.setSelectedCurrency(currencyName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                loadCurrencies()
            }
            .also { delegate.addDisposable(it) }
    }
}