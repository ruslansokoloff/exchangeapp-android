package ru.ruslansokolov.exchangeapp.presentation.entities

import ru.ruslansokolov.core.presentation.text_config.ResourceTextConfig
import ru.ruslansokolov.exchangeapp.R
import ru.ruslansokolov.exchangeapp.data.entities.Currency
import ru.ruslansokolov.exchangeapp.presentation.currency_adapter.CurrencyItem
import javax.inject.Inject

class CurrenciesListStateMapper @Inject constructor() {

    operator fun invoke(currencies: List<Currency>): CurrenciesListState {
        return if(currencies.isNotEmpty()) {
            CurrenciesListState.Success(currencies.map { CurrencyItem(it) })
        } else {
            CurrenciesListState.Error(ResourceTextConfig(R.string.err_empty_currencies))
        }
    }
}