package ru.ruslansokolov.exchangeapp.presentation.currency_adapter

import androidx.lifecycle.MutableLiveData
import ru.ruslansokolov.core.R
import ru.ruslansokolov.exchangeapp.data.entities.Currency

class CurrencyItem(val currency: Currency) {

    val isFavorite: MutableLiveData<Boolean> = MutableLiveData(currency.favorite)

    val displayName: String
        get() = currency.name

    val displayRate: String?
        get() = currency.rate?.toString()

    val nameTextColorRes: Int
        get() = if (currency.selected) R.color.purple_500 else R.color.black
}