package ru.ruslansokolov.exchangeapp.presentation.activity

import android.os.Bundle
import ru.ruslansokolov.core.presentation.activity.BaseActivity
import ru.ruslansokolov.exchangeapp.BR
import ru.ruslansokolov.exchangeapp.R
import ru.ruslansokolov.exchangeapp.appComponent
import ru.ruslansokolov.exchangeapp.databinding.ActivityMainBinding

class MainActivity :
    BaseActivity<MainActivityViewModel, ActivityMainBinding>(MainActivityViewModel::class.java) {

    override val layoutIdRes: Int = R.layout.activity_main

    override val fragmentsContainerId: Int = R.id.fragments_container

    override val viewModelVariableId: Int = BR.viewModel

    override fun inject() {
        appComponent().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBottomMenu()
    }

    private fun initBottomMenu() {
        binding.bottomNavigationView.setOnItemSelectedListener {
            viewModel.onMenuItemClicked(it.itemId)
            return@setOnItemSelectedListener true
        }
    }
}