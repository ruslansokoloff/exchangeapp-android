package ru.ruslansokolov.exchangeapp.presentation.currency_adapter

import android.view.ViewGroup
import ru.ruslansokolov.core.presentation.adapter.BaseBindingAdapter
import ru.ruslansokolov.exchangeapp.R

class CurrencyAdapter : BaseBindingAdapter<CurrencyItem, CurrencyViewHolder>() {

    var favoriteClickListener: ((CurrencyItem) -> Unit)? = null
    var selectClickListener: ((CurrencyItem) -> Unit)? = null

    init {
        diffUtilCallback = CurrencyDiffUtilCallback()
    }

    override fun createViewHolderInstance(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        return CurrencyViewHolder(inflate(parent, R.layout.item_currency))
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        val item = getItem(position)
        holder.binding.favoriteButton.setOnClickListener {
            item.isFavorite.value = !(item.isFavorite.value ?: false)
            favoriteClickListener?.invoke(item)
        }
        holder.binding.root.setOnClickListener {
            selectClickListener?.invoke(item)
        }
    }
}