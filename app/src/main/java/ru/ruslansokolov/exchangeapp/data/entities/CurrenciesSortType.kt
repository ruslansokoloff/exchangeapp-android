package ru.ruslansokolov.exchangeapp.data.entities

enum class CurrenciesSortType {
    BY_NAME_ASC,
    BY_NAME_DESC,
    BY_RATE_ASC,
    BY_RATE_DESC;

    fun getOrderField(): String {
        return when (this) {
            BY_NAME_ASC, BY_NAME_DESC -> CurrencyDb.NAME
            BY_RATE_ASC, BY_RATE_DESC -> CurrencyDb.RATE
        }
    }

    fun getIsAsc(): Boolean {
        return when (this) {
            BY_NAME_ASC, BY_RATE_ASC -> true
            BY_NAME_DESC, BY_RATE_DESC -> false
        }
    }

    companion object {
        val DEFAULT = BY_NAME_ASC
    }
}