package ru.ruslansokolov.exchangeapp.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = CurrencySelectedDb.TABLE_NAME)
class CurrencySelectedDb {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID)
    var id: Int = 0

    @ColumnInfo(name = CURRENCY_NAME)
    var currencyName: String? = null

    companion object {
        const val TABLE_NAME = "currencySelected"
        const val ID = "id"
        const val CURRENCY_NAME = "currencyName"
    }
}