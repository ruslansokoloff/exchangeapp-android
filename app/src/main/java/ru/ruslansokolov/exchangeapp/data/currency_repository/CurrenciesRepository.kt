package ru.ruslansokolov.exchangeapp.data.currency_repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.ruslansokolov.exchangeapp.data.entities.CurrenciesSortType
import ru.ruslansokolov.exchangeapp.data.entities.Currency

interface CurrenciesRepository {

    fun getAll(forceUpdate: Boolean, sortType: CurrenciesSortType): Single<List<Currency>>

    fun getFavorites(): Single<List<Currency>>

    fun setFavorite(name: String, isFavorite: Boolean): Completable

    fun getSelectedCurrency(): Single<Currency>

    fun setSelectedCurrency(name: String): Completable
}