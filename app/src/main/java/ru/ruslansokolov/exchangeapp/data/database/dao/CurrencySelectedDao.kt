package ru.ruslansokolov.exchangeapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import ru.ruslansokolov.exchangeapp.data.entities.CurrencySelectedDb

@Dao
interface CurrencySelectedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favorite: CurrencySelectedDb): Completable

    @Query("DELETE FROM ${CurrencySelectedDb.TABLE_NAME}")
    fun clear(): Completable
}