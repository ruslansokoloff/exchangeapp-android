package ru.ruslansokolov.exchangeapp.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = CurrencyDb.TABLE_NAME)
class CurrencyDb {

    @PrimaryKey
    @ColumnInfo(name = NAME)
    var name: String = ""

    @ColumnInfo(name = RATE)
    var rate: Double? = null

    companion object {
        const val TABLE_NAME = "currencies"
        const val NAME = "name"
        const val RATE = "rate"
        const val IS_BASE = "isBase"
    }
}