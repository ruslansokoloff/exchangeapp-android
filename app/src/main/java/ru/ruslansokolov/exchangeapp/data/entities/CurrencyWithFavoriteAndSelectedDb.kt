package ru.ruslansokolov.exchangeapp.data.entities

import androidx.room.Embedded
import androidx.room.Relation

class CurrencyWithFavoriteAndSelectedDb {

    @Embedded
    lateinit var currency: CurrencyDb

    @Relation(
        entity = CurrencyFavoriteDb::class,
        parentColumn = CurrencyDb.NAME,
        entityColumn = CurrencyFavoriteDb.CURRENCY_NAME
    )
    var favorite: CurrencyFavoriteDb? = null

    @Relation(
        entity = CurrencySelectedDb::class,
        parentColumn = CurrencyDb.NAME,
        entityColumn = CurrencySelectedDb.CURRENCY_NAME
    )
    var selected: CurrencySelectedDb? = null
}