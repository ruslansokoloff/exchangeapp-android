package ru.ruslansokolov.exchangeapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.ruslansokolov.exchangeapp.data.database.dao.CurrencyDao
import ru.ruslansokolov.exchangeapp.data.database.dao.CurrencyFavoriteDao
import ru.ruslansokolov.exchangeapp.data.database.dao.CurrencySelectedDao
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyFavoriteDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencySelectedDb

@Database(
    entities = [
        CurrencyDb::class,
        CurrencyFavoriteDb::class,
        CurrencySelectedDb::class,
    ], version = AppDatabase.DB_VERSION
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun currencyDao(): CurrencyDao
    abstract fun currencyFavoriteDao(): CurrencyFavoriteDao
    abstract fun currencySelectedDao(): CurrencySelectedDao

    companion object {
        const val DB_NAME = "app_database"
        const val DB_VERSION = 5
    }
}