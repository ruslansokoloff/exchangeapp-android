package ru.ruslansokolov.exchangeapp.data.currency_repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.ruslansokolov.exchangeapp.data.database.dao.CurrencyDao
import ru.ruslansokolov.exchangeapp.data.database.dao.CurrencyFavoriteDao
import ru.ruslansokolov.exchangeapp.data.database.dao.CurrencySelectedDao
import ru.ruslansokolov.exchangeapp.data.entities.CurrenciesSortType
import ru.ruslansokolov.exchangeapp.data.entities.Currency
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyFavoriteDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencySelectedDb
import ru.ruslansokolov.exchangeapp.data.mapper.CurrencyMapper
import ru.ruslansokolov.exchangeapp.data.network.CurrenciesApi

class CurrenciesRepositoryImpl(
    private val mapper: CurrencyMapper,
    private val dao: CurrencyDao,
    private val favoriteDao: CurrencyFavoriteDao,
    private val selectedDao: CurrencySelectedDao,
    private val api: CurrenciesApi,
) : CurrenciesRepository {

    override fun getAll(
        forceUpdate: Boolean,
        sortType: CurrenciesSortType
    ): Single<List<Currency>> {
        val updateCompletable = if (forceUpdate) {
            getSelectedCurrency()
                .flatMap { selectedCurrency ->
                    api.getLatest()
                        .map {
                            mapper.fromApi(it, selectedCurrency.name)
                        }
                }
                .flatMapCompletable(this::updateAll)
        } else {
            Completable.complete()
        }

        return updateCompletable
            .andThen(dao.getAll())
            .map { it.map(mapper::fromDb) }
            .map { sortCurrencies(it, sortType) }
    }

    override fun getFavorites(): Single<List<Currency>> {
        return dao.getFavorites()
            .map { it.map(mapper::fromDb) }
    }

    override fun setFavorite(name: String, isFavorite: Boolean): Completable {
        return if (isFavorite) {
            val favorite = CurrencyFavoriteDb().apply {
                currencyName = name
            }
            favoriteDao.insert(favorite)
        } else {
            favoriteDao.remove(name)
        }
    }

    override fun getSelectedCurrency(): Single<Currency> {
        return dao.getSelectedCurrency()
            .map(mapper::fromDb)
            .toSingle(Currency.EMPTY)
    }

    override fun setSelectedCurrency(name: String): Completable {
        val selected = CurrencySelectedDb().apply {
            currencyName = name
        }
        return updateSelected(selected)
    }

    private fun updateAll(currencies: List<Currency>): Completable {
        val selected = CurrencySelectedDb().apply {
            currencyName = currencies.firstOrNull { it.selected }?.name
        }
        return dao.clear()
            .andThen(updateSelected(selected))
            .andThen(dao.insertAll(currencies.map(mapper::toDb)))
    }

    private fun updateSelected(selected: CurrencySelectedDb): Completable {
        return selectedDao.clear()
            .andThen(selectedDao.insert(selected))
    }

    private fun sortCurrencies(list: List<Currency>, sortType: CurrenciesSortType): List<Currency> {
        return when (sortType) {
            CurrenciesSortType.BY_NAME_ASC -> list.sortedBy { it.name }
            CurrenciesSortType.BY_NAME_DESC -> list.sortedByDescending { it.name }
            CurrenciesSortType.BY_RATE_ASC -> list.sortedBy { it.rate }
            CurrenciesSortType.BY_RATE_DESC -> list.sortedByDescending { it.rate }
        }
    }
}