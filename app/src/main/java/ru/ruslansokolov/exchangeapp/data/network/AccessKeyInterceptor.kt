package ru.ruslansokolov.exchangeapp.data.network

import okhttp3.Interceptor
import okhttp3.Response
import ru.ruslansokolov.exchangeapp.BuildConfig

class AccessKeyInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url
            .newBuilder()
            .addQueryParameter(ACCESS_KEY, BuildConfig.API_ACCESS_KEY)
            .build()

        return chain.proceed(request.newBuilder().url(url).build())
    }

    private companion object {
        const val ACCESS_KEY = "access_key"
    }
}