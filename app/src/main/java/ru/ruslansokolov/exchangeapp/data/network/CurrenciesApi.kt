package ru.ruslansokolov.exchangeapp.data.network

import io.reactivex.Single
import retrofit2.http.GET
import ru.ruslansokolov.exchangeapp.data.entities.CurrenciesResponse

interface CurrenciesApi {

    @GET("latest")
    fun getLatest(): Single<CurrenciesResponse>
}