package ru.ruslansokolov.exchangeapp.data.mapper

import ru.ruslansokolov.exchangeapp.data.entities.CurrenciesResponse
import ru.ruslansokolov.exchangeapp.data.entities.Currency
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyWithFavoriteAndSelectedDb

interface CurrencyMapper {

    fun fromDb(db: CurrencyWithFavoriteAndSelectedDb): Currency

    fun toDb(currency: Currency): CurrencyDb

    fun fromApi(response: CurrenciesResponse, selectedName: String): List<Currency>
}

class CurrencyMapperImpl : CurrencyMapper {

    override fun fromDb(db: CurrencyWithFavoriteAndSelectedDb): Currency {
        return Currency(
            name = db.currency.name,
            rate = db.currency.rate,
            favorite = db.favorite != null,
            selected = db.selected != null,
        )
    }

    override fun toDb(currency: Currency): CurrencyDb {
        return CurrencyDb().apply {
            name = currency.name
            rate = currency.rate
        }
    }

    override fun fromApi(response: CurrenciesResponse, selectedName: String): List<Currency> {
        return response.rates?.map {
            val isSelected = if (selectedName.isEmpty())
                it.key == response.base
            else
                selectedName == it.key

            Currency(
                name = it.key,
                rate = it.value,
                selected = isSelected,
                favorite = false,
            )
        } ?: emptyList()
    }
}