package ru.ruslansokolov.exchangeapp.data.entities

data class Currency(
    val name: String,
    val rate: Double?,
    val selected: Boolean,
    val favorite: Boolean,
) {

    companion object {
        val EMPTY = Currency("", 0.0, false, false)
    }
}