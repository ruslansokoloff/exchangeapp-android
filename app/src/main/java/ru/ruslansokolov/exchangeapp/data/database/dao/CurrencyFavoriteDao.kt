package ru.ruslansokolov.exchangeapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyFavoriteDb

@Dao
interface CurrencyFavoriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favorite: CurrencyFavoriteDb): Completable

    @Query("DELETE FROM ${CurrencyFavoriteDb.TABLE_NAME} WHERE ${CurrencyFavoriteDb.CURRENCY_NAME} = :currencyName")
    fun remove(currencyName: String): Completable
}