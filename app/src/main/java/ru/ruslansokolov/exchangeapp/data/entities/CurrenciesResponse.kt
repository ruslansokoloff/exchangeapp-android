package ru.ruslansokolov.exchangeapp.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CurrenciesResponse {

    @Expose
    @SerializedName(value = "base")
    var base: String? = null

    @Expose
    @SerializedName(value = "rates")
    var rates: Map<String, Double?>? = null
}