package ru.ruslansokolov.exchangeapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyFavoriteDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencySelectedDb
import ru.ruslansokolov.exchangeapp.data.entities.CurrencyWithFavoriteAndSelectedDb

@Dao
interface CurrencyDao {

    @Query("SELECT * FROM ${CurrencyDb.TABLE_NAME}")
    fun getAll(): Single<List<CurrencyWithFavoriteAndSelectedDb>>

    @Query(
        "SELECT * FROM ${CurrencyDb.TABLE_NAME} " +
                "INNER JOIN ${CurrencyFavoriteDb.TABLE_NAME} " +
                "ON ${CurrencyDb.NAME} = ${CurrencyFavoriteDb.CURRENCY_NAME} " +
                "ORDER BY ${CurrencyDb.NAME}"
    )
    fun getFavorites(): Single<List<CurrencyWithFavoriteAndSelectedDb>>

    @Query(
        "SELECT * FROM ${CurrencyDb.TABLE_NAME} " +
                "INNER JOIN ${CurrencySelectedDb.TABLE_NAME} " +
                "ON ${CurrencyDb.NAME} = ${CurrencySelectedDb.CURRENCY_NAME} " +
                "ORDER BY ${CurrencyDb.NAME}"
    )
    fun getSelectedCurrency(): Maybe<CurrencyWithFavoriteAndSelectedDb>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entities: List<CurrencyDb>): Completable

    @Query("DELETE FROM ${CurrencyDb.TABLE_NAME}")
    fun clear(): Completable
}