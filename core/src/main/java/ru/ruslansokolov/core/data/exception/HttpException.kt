package ru.ruslansokolov.core.data.exception

sealed class HttpException : Exception() {

    object EmptyBody : HttpException()

    object NetworkDisabled : HttpException()

    object Unknown : HttpException()
}