package ru.ruslansokolov.core.presentation.text_config

import android.content.Context

class StringTextConfig(private val str: CharSequence?) : TextConfig {

    override fun getString(context: Context): CharSequence? = str
}