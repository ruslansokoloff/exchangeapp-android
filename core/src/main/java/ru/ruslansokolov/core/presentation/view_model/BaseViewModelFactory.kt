package ru.ruslansokolov.core.presentation.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class BaseViewModelFactory<VM : BaseViewModel<out BaseViewModelDelegate>> : ViewModelProvider.Factory {

    abstract fun provideViewModel(): VM

    override fun <T : ViewModel> create(modelClass: Class<T>): T = provideViewModel() as T
}