package ru.ruslansokolov.core.presentation.fragment

import ru.ruslansokolov.core.presentation.view_model.BaseViewModel
import ru.ruslansokolov.core.presentation.view_model.BaseViewModelDelegate

abstract class FragmentViewModel<D : BaseViewModelDelegate> : BaseViewModel<D>()