package ru.ruslansokolov.core.presentation.view_model

abstract class BaseViewModelDelegate {

    open fun onResume() {}

    open fun onStart() {}

    open fun onPause() {}

    open fun onStop() {}

    open fun onCleared() {}
}