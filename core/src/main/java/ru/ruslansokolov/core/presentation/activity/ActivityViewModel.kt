package ru.ruslansokolov.core.presentation.activity

import ru.ruslansokolov.core.presentation.view_model.BaseViewModel
import ru.ruslansokolov.core.presentation.view_model.BaseViewModelDelegate

abstract class ActivityViewModel<D : BaseViewModelDelegate> : BaseViewModel<D>()