package ru.ruslansokolov.core.presentation.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.*
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.inc_toolbar.view.*
import ru.ruslansokolov.core.R
import ru.ruslansokolov.core.presentation.view_model.BaseViewModelDelegate
import ru.ruslansokolov.core.presentation.view_model.BaseViewModelFactory
import javax.inject.Inject
import javax.inject.Provider

abstract class BaseFragment<VM : FragmentViewModel<out BaseViewModelDelegate>, B : ViewDataBinding>(
    private val viewModelType: Class<VM>
) : Fragment() {

    @Inject
    lateinit var viewModelProvider: Provider<VM>

    @get:LayoutRes
    abstract val layoutResId: Int

    abstract val viewModelVariableId: Int

    val viewModel: VM by lazy {
        getViewModelInstance()
    }

    protected lateinit var binding: B

    abstract fun inject()

    @CallSuper
    override fun onAttach(context: Context) {
        super.onAttach(context)
        inject()
    }

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lifecycle.addObserver(viewModel)

        binding = DataBindingUtil.inflate(inflater, layoutResId, container, false)
        binding.setVariable(viewModelVariableId, viewModel)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }


    protected fun setToolbarTitle(title: String?) {
        binding.root.findViewById<Toolbar>(R.id.toolbar)
        binding.root.toolbar?.title = title
    }

    protected fun setToolbarTitle(@StringRes resId: Int) {
        setToolbarTitle(getString(resId))
    }

    protected fun setToolbarMenu(@MenuRes resId: Int) {
        binding.root.toolbar?.apply {
            inflateMenu(resId)
            setOnMenuItemClickListener {
                onOptionsItemSelected(it)
            }
        }
    }

    protected fun setToolbarNavigationIcon(
        @DrawableRes resId: Int,
        callback: (() -> Boolean)? = null
    ) {
        binding.root.toolbar?.apply {
            setNavigationIcon(resId)
            setNavigationOnClickListener {
                val wasHandled = callback?.invoke() ?: false
                if (!wasHandled) {
                    activity?.onBackPressed()
                }
            }
        }
    }


    private fun getViewModelInstance(): VM {
        return ViewModelProvider(this, getViewModelFactoryInstance()).get(viewModelType)
    }

    private fun getViewModelFactoryInstance(): BaseViewModelFactory<VM> {
        return object : BaseViewModelFactory<VM>() {
            override fun provideViewModel(): VM {
                return viewModelProvider.get().also {
                    it.onViewModelCreated(arguments)
                }
            }
        }
    }
}