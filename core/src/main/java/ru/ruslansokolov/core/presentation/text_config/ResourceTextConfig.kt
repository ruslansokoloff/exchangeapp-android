package ru.ruslansokolov.core.presentation.text_config

import android.content.Context
import androidx.annotation.StringRes

class ResourceTextConfig(
    @StringRes private val stringResId: Int
) : TextConfig {

    override fun getString(context: Context): String {
        return context.getString(stringResId)
    }
}