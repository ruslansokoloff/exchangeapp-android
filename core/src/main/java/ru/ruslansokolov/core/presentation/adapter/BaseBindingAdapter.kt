package ru.ruslansokolov.core.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil

abstract class BaseBindingAdapter<Model, VH : BaseBindingViewHolder<Model, out ViewDataBinding>> : LiveDataBindingRecyclerViewAdapter<VH>() {

    private var items: List<Model> = mutableListOf()

    var diffUtilCallback: BaseDiffUtilCallback<Model>? = null

    abstract fun createViewHolderInstance(parent: ViewGroup, viewType: Int): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return createViewHolderInstance(parent, viewType).apply {
            binding.lifecycleOwner = this
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    fun getItem(position: Int): Model {
        return items[position]
    }

    fun <Binding : ViewDataBinding> inflate(parent: ViewGroup, layoutResId: Int): Binding =
            DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    layoutResId,
                    parent,
                    false
            )

    open fun updateItems(newItems: List<Model>) {
        val callback = diffUtilCallback
        if (callback != null) {
            DiffUtil.calculateDiff(callback.apply {
                setLists(items, newItems)
            }).also {
                updateList(newItems)
                it.dispatchUpdatesTo(this)
            }
        } else {
            updateList(newItems)
        }
    }

    private fun updateList(newItems: List<Model>) {
        items = newItems
    }
}