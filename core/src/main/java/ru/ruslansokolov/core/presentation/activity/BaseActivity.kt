package ru.ruslansokolov.core.presentation.activity

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.github.terrakok.cicerone.Navigator
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.androidx.AppNavigator
import ru.ruslansokolov.core.presentation.view_model.BaseViewModelDelegate
import ru.ruslansokolov.core.presentation.view_model.BaseViewModelFactory
import javax.inject.Inject
import javax.inject.Provider

abstract class BaseActivity<VM : ActivityViewModel<out BaseViewModelDelegate>, B : ViewDataBinding>(
    private val viewModelType: Class<VM>
) : AppCompatActivity() {

    @Inject
    lateinit var viewModelProvider: Provider<VM>

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @get:LayoutRes
    abstract val layoutIdRes: Int

    @get:IdRes
    abstract val fragmentsContainerId: Int

    abstract val viewModelVariableId: Int

    val viewModel: VM by lazy {
        getViewModelInstance()
    }

    protected lateinit var binding: B

    private lateinit var navigator: Navigator

    abstract fun inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()

        lifecycle.addObserver(viewModel)

        binding = DataBindingUtil.setContentView(this, layoutIdRes)
        binding.setVariable(viewModelVariableId, viewModel)
        binding.lifecycleOwner = this

        navigator = AppNavigator(this, fragmentsContainerId)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    protected open fun getViewModelInstance(): VM {
        return ViewModelProvider(this, getViewModelFactoryInstance()).get(viewModelType)
    }

    private fun getViewModelFactoryInstance(): BaseViewModelFactory<VM> {
        return object : BaseViewModelFactory<VM>() {
            override fun provideViewModel(): VM {
                return viewModelProvider.get().also {
                    it.onViewModelCreated(intent.extras)
                }
            }
        }
    }
}