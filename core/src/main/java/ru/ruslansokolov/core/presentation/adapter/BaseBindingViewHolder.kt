package ru.ruslansokolov.core.presentation.adapter

import android.content.Context
import androidx.databinding.ViewDataBinding

abstract class BaseBindingViewHolder<Model, Binding : ViewDataBinding>(val binding: Binding) :
    LiveDataBindingRecyclerViewAdapter.LiveDataBindingViewHolder(binding.root) {

    abstract val viewModelVariableId: Int

    protected val context: Context = binding.root.context

    open fun bind(model: Model) {
        binding.setVariable(viewModelVariableId, model)
    }
}