package ru.ruslansokolov.core.presentation.view_model

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.lifecycle.*
import ru.ruslansokolov.core.R
import ru.ruslansokolov.core.data.exception.HttpException
import ru.ruslansokolov.core.presentation.text_config.ResourceTextConfig
import ru.ruslansokolov.core.presentation.text_config.StringTextConfig
import ru.ruslansokolov.core.presentation.text_config.TextConfig
import javax.inject.Inject

abstract class BaseViewModel<D : BaseViewModelDelegate> : ViewModel(), LifecycleObserver {

    @Inject
    protected lateinit var delegate: D

    open fun onViewModelCreated(args: Bundle?) {}

    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {
        delegate.onResume()
    }

    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause() {
        delegate.onPause()
    }

    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
        delegate.onStart()
    }

    @CallSuper
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
        delegate.onStop()
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        delegate.onCleared()
    }

    protected fun parseThrowable(
        throwable: Throwable,
        defaultErrorConfig: TextConfig? = null,
        defaultConfigs: Map<Class<*>, TextConfig> = emptyMap()
    ): TextConfig {
        val defaultConfig = defaultErrorConfig ?: StringTextConfig("")

        if (defaultConfigs.containsKey(throwable.javaClass)) {
            return defaultConfigs[throwable.javaClass] ?: defaultConfig
        }
        if (throwable is HttpException) {
            return parseHttpException(throwable)
        }
        return defaultConfig
    }

    private fun parseHttpException(e: HttpException): TextConfig {
        return when (e) {
            is HttpException.NetworkDisabled -> ResourceTextConfig(R.string.err_network_disabled)
            is HttpException.EmptyBody -> ResourceTextConfig(R.string.err_no_data)
            is HttpException.Unknown -> ResourceTextConfig(R.string.err_unknown)
        }
    }
}