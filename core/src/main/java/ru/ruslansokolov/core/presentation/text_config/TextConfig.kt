package ru.ruslansokolov.core.presentation.text_config

import android.content.Context

interface TextConfig {
    fun getString(context: Context): CharSequence?
}